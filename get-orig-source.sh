#!/bin/sh

set -e

PKG_NAME=`dpkg-parsechangelog | awk '/^Source:/ {print $2}'`
PKG_VERSION=`dpkg-parsechangelog | awk '/^Version:/ {print $2}'`
UP_VERSION=`dpkg-parsechangelog | \
    awk '/^Version:/ {gsub(/\+ds.*/, "", $2); print $2}'`
DFSG=`dpkg-parsechangelog | \
    awk '/^Version:/ {gsub(/.*\+ds/, "+ds", $2); gsub(/-.*/, "", $2); print $2}'`
ORIG_DIR="$PKG_NAME-$UP_VERSION$DFSG.orig"
ORIG_NAME="${PKG_NAME}_$UP_VERSION$DFSG.orig"
UP_DIR="`pwd`/../tarballs"
UP_DISTDIR="$UP_DIR/$PKG_NAME"
UP_SITE='ftp://ftp.netbsd.org/pub/NetBSD/packages/distfiles/LOCAL_PORTS/'
UP_DISTVER=`echo "$UP_VERSION" | tr -d '.'`
UP_DISTNAME="$PKG_NAME-$UP_DISTVER"
UP_TARBALL="$UP_DISTNAME.tar.gz"
RM_FILES='presentations ref'

# Fetch the upstream tarball if needed
echo "up $UP_TARBALL site $UP_SITE orig $ORIG_NAME"
mkdir -p "$UP_DIR"
if [ ! -f "$UP_DIR/$UP_TARBALL" ]; then
	echo "Fetching upstream tarball $UP_DIR/$UP_TARBALL"
	(cd "$UP_DIR" && wget -nv -c "$UP_SITE$UP_TARBALL")
else
	echo "Upstream tarball $UP_DIR/$UP_TARBALL exists";
fi

# Extract it
rm -rf "$UP_DIR/$UP_DISTNAME" "$UP_DIR/$ORIG_DIR" "$UP_DIR/blank"
(cd "$UP_DIR" && tar zxf "$UP_TARBALL")

# Do our magic
(cd "$UP_DIR/$UP_DISTNAME" && rm -rfv $RM_FILES)
(cd "$UP_DIR/$UP_DISTNAME" && find . \( -name Makefile -or -name config.log -or -name config.status -or -name libtool -or -name config.h -or -name .deps -or -name atconfig -or -name atlocal -or -name stamp-h1 \) -exec rm -rfv {} + )
mv "$UP_DIR/$UP_DISTNAME" "$UP_DIR/$ORIG_DIR"

# Create the repackaged tarball
(cd "$UP_DIR" && tar -cf - "$ORIG_DIR" | gzip -cn9 > "$ORIG_NAME.tar.gz")
echo "The repackaged source is at $UP_DIR/$ORIG_NAME.tar.gz"
